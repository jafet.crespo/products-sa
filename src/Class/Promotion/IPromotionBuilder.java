package Class.Promotion;

public interface IPromotionBuilder {
    void setShipping(double shipping);
    void setDiscount(double discount);
    void setPaidMethod(double paidMethod);
}
