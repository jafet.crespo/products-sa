package Class.Promotion;

public class Promotion {
    private final double shipping;
    private final double discount;
    private final double paidMethod;

    public Promotion(double shipping, double discount, double paidMethod) {
        this.shipping = shipping;
        this.discount = discount;
        this.paidMethod = paidMethod;
    }

    public double totalDiscount(){
        return this.shipping + this.discount + this.paidMethod;
    }
    public void showDiscount() {
        System.out.println("El descuento total es: " + this.totalDiscount() * 100 + "%");
    }
}
