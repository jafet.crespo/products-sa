package Class.Promotion.Builder;

import Class.Promotion.IPromotionBuilder;

public class DirectorPromotion {
    public void christmasPromotion(IPromotionBuilder promotionBuilder){
        promotionBuilder.setDiscount(0.15);
        promotionBuilder.setShipping(0.03);
        promotionBuilder.setPaidMethod(0.02);
    }
    public void halloweenPromotion(IPromotionBuilder promotionBuilder){
        promotionBuilder.setDiscount(0.10);
        promotionBuilder.setPaidMethod(0.02);
    }

    public void withoutPromotion(IPromotionBuilder promotionBuilder){
        promotionBuilder.setDiscount(0);
        promotionBuilder.setShipping(0);
        promotionBuilder.setPaidMethod(0);
    }

}
