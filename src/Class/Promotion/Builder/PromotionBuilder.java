package Class.Promotion.Builder;

import Class.Promotion.IPromotionBuilder;
import Class.Promotion.Promotion;

public class PromotionBuilder implements IPromotionBuilder {

    private double shipping;
    private double discount;
    private double paidMethod;

    public PromotionBuilder() {
        this.shipping = 0;
        this.discount = 0;
        this.paidMethod = 0;
    }

    @Override
    public void setShipping(double shipping) {
        this.shipping = shipping;
    }

    @Override
    public void setDiscount(double discount) {
        this.discount = discount;
    }

    @Override
    public void setPaidMethod(double paidMethod) {
        this.paidMethod = paidMethod;
    }

    public Promotion getPromotion() {
        return new Promotion(this.shipping, this.discount, this.paidMethod);
    }
}
