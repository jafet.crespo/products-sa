package Class.Purchase;

public interface IPurchaseMethod {
    void payTheSeller();
    void receiveChangeBill();
}
