package Class.Purchase.Adapter;

import Class.Purchase.IPurchaseMethod;

public class FaceToFacePurchase implements IPurchaseMethod {
    @Override
    public void payTheSeller() {
        System.out.println("Acercase al vendedor y pagar por el producto elegido");
    }

    @Override
    public void receiveChangeBill() {
        System.out.println("Recibir el cambio y la factura de compra");
    }
}
