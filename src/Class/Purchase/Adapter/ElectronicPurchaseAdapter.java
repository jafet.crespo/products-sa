package Class.Purchase.Adapter;

import Class.Purchase.IElectronicPurchase;
import Class.Purchase.IPurchaseMethod;

public class ElectronicPurchaseAdapter implements IPurchaseMethod {
    IElectronicPurchase electronicPurchase;

    public ElectronicPurchaseAdapter(IElectronicPurchase electronicPurchase) {
        this.electronicPurchase = electronicPurchase;
    }

    @Override
    public void payTheSeller() {
        electronicPurchase.verifyCard();
        electronicPurchase.deductMoney();
    }

    @Override
    public void receiveChangeBill() {
        electronicPurchase.electronicBill();
    }
}
