package Class.Purchase.Adapter;

import Class.Purchase.IElectronicPurchase;

public class OnlinePurchase implements IElectronicPurchase {
    @Override
    public void verifyCard() {
        System.out.println("Ingresar los datos de la tarjeta o método de pago electrónico");
    }

    @Override
    public void deductMoney() {
        System.out.println("Descontando dinero por la compra de un producto");
    }

    @Override
    public void electronicBill() {
        System.out.println("Mostrar factura de compra y enviar un email con los datos de compra");
    }
}
