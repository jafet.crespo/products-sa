package Class.Purchase;

public interface IElectronicPurchase {
    void verifyCard();
    void deductMoney();
    void electronicBill();
}
