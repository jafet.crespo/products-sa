package Class.Product.PBuilder;

import java.time.LocalDate;

public interface IProductDTOBuilder {
    void setId(long id);
    void setName(String name);
    void setPrice(double price);
    void setDescription(String description);
    void setType(String type);
    void setSellByDate(LocalDate sellByDate);
}
