package Class.Product.PBuilder;

import Class.Product.Composite.ProductDTO;

import java.time.LocalDate;

public class ProductDTOBuilder implements IProductDTOBuilder {
    private long id;
    private String name;
    private double price;
    private String description;
    private String type;
    private LocalDate sellByDate;

    @Override
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void setSellByDate(LocalDate sellByDate) {
        this.sellByDate = sellByDate;
    }

    public ProductDTO getProduct() {
        return new ProductDTO(this.id, this.name, this.price, this.description, this.type, this.sellByDate);
    }
}
