package Class.Product.PBuilder;


import java.time.LocalDate;
import java.util.Scanner;

public class DirectorProduct {
    public void defaultProduct(IProductDTOBuilder productDTOBuilder) {
        productDTOBuilder.setId(1);
        productDTOBuilder.setName("default product");
        productDTOBuilder.setDescription("este es un producto de prueba");
        productDTOBuilder.setPrice(100);
        productDTOBuilder.setType("pruebas");
        productDTOBuilder.setSellByDate(LocalDate.now().plusYears(10));
    }

    public void personalProduct(IProductDTOBuilder productDTOBuilder) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("ingrese un id: ");
        long id = scanner.nextInt();
        System.out.println("ingrese un nombre: ");
        String name = scanner.next();
        System.out.println("ingrese el precio: ");
        float price = scanner.nextFloat();
        System.out.println("ingrese una descripción: ");
        String description = scanner.next();
        System.out.println("ingrese un tipo de producto: ");
        String type = scanner.next();
        productDTOBuilder.setId(id);
        productDTOBuilder.setName(name);
        productDTOBuilder.setDescription(description);
        productDTOBuilder.setPrice(price);
        productDTOBuilder.setType(type);
        productDTOBuilder.setSellByDate(LocalDate.now().plusYears(10));
    }
}
