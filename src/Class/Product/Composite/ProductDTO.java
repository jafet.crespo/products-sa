package Class.Product.Composite;

import java.time.LocalDate;

public class ProductDTO implements IProduct {
    private final long id;
    private final String name;
    private final double price;
    private final String description;
    private final String type;
    private final LocalDate sellByDate;

    public ProductDTO(long id, String name, double price, String description, String type, LocalDate sellByDate) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
        this.type = type;
        this.sellByDate = sellByDate;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public LocalDate getSellByDate() {
        return sellByDate;
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", type='" + type + '\'' +
                ", sellByDate=" + sellByDate +
                '}';
    }

    @Override
    public void getProduct() {
        System.out.println(this);
    }
}
