package Class.Product.Composite;

public class ProductGift implements IProduct {

    private final String name;
    private final String description;

    public ProductGift(String name, String description) {
        this.name = name;
        this.description = description;
    }

    @Override
    public String toString() {
        return "ProductGift{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public void getProduct() {
        System.out.println(this);
    }
}
