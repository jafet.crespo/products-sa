package Class.Product.Composite;

import java.util.ArrayList;

public class ProductDTOComposite implements IProduct {

    private final ArrayList<IProduct> productList;

    public ProductDTOComposite() {
        this.productList = new ArrayList<>();
    }

    @Override
    public void getProduct() {
        productList.forEach(IProduct::getProduct);
    }

    public void addProduct(IProduct product){
        productList.add(product);
    }
}
