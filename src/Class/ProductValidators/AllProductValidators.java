package Class.ProductValidators;

import Class.Product.Composite.ProductDTO;
import Class.ProductValidators.ChainResposability.NameValidator;
import Class.ProductValidators.ChainResposability.PriceValidator;
import Class.ProductValidators.ChainResposability.SellByDateValidator;

public class AllProductValidators {
    private final IProductValidator validator;

    public AllProductValidators() {
        IProductValidator nameValidator = new NameValidator();
        IProductValidator priceValidator = new PriceValidator();
        IProductValidator sellByDateValidator = new SellByDateValidator();
        nameValidator.nextValidator(priceValidator);
        priceValidator.nextValidator(sellByDateValidator);

        this.validator = nameValidator;
    }

    public boolean validateProduct(ProductDTO product){
        return this.validator.validate(product);
    }
}
