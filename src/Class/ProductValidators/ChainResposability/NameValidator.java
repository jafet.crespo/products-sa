package Class.ProductValidators.ChainResposability;

import Class.Product.Composite.ProductDTO;

public class NameValidator extends ProductValidator  {
    @Override
    public boolean validate(ProductDTO product) {
        if (product.getName().length() > 50) {
            System.out.println("Nombre incorrecto!!!");
            return false;
        }
        return super.validate(product);
    }
}
