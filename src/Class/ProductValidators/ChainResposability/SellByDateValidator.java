package Class.ProductValidators.ChainResposability;

import Class.Product.Composite.ProductDTO;

import java.time.LocalDate;

public class SellByDateValidator extends ProductValidator {
    @Override
    public boolean validate(ProductDTO product) {
        if (product.getSellByDate().isBefore(LocalDate.now())) {
            System.out.println("Producto caducado!!!");
            return false;
        }

        return super.validate(product);
    }
}
