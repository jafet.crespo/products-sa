package Class.ProductValidators.ChainResposability;

import Class.Product.Composite.ProductDTO;
import Class.ProductValidators.IProductValidator;

public abstract class ProductValidator implements IProductValidator {
    IProductValidator nextProductValidator;
    @Override
    public boolean validate(ProductDTO product){
        if (nextProductValidator != null) return this.nextProductValidator.validate(product);
        return true;
    }
    @Override
    public void nextValidator(IProductValidator validator){
        this.nextProductValidator = validator;
    }
}
