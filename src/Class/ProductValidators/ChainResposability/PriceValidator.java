package Class.ProductValidators.ChainResposability;

import Class.Product.Composite.ProductDTO;

public class PriceValidator extends ProductValidator {
    @Override
    public boolean validate(ProductDTO product) {
        if (product.getPrice() < 0) {
            System.out.println("Precio incorrecto!!!");
            return false;
        }
        return super.validate(product);
    }
}
