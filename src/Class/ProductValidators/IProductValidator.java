package Class.ProductValidators;

import Class.Product.Composite.ProductDTO;

public interface IProductValidator {
    boolean validate(ProductDTO product);
    void nextValidator(IProductValidator validator);
}
