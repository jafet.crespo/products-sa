package Class.DB;

import Class.Product.Composite.ProductDTO;

import java.util.ArrayList;

public class ArrayDB implements IDatabase {

    ArrayList<ProductDTO> products;

    public ArrayDB() {
        this.products = new ArrayList<>();
    }

    @Override
    public void connect() {
        System.out.println("conexion a base de datos de array!!!");
    }

    @Override
    public void insert(ProductDTO product) {
        System.out.println("Insertando producto a la base de datos");
        products.add(product);
    }
}
