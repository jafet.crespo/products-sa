package Class.DB;

import Class.Product.Composite.ProductDTO;

public class TextDB implements IDatabase {
    private String products;
    public TextDB() {
        this.products = "";
    }

    @Override
    public void connect() {
        System.out.println("conexion a base de datos de texto!!");
    }

    @Override
    public void insert(ProductDTO product) {
        System.out.println("Insertando producto a la base de datos");
        this.products = this.products.concat(product.toString() + ",");
    }

}
