package Class.DB.Template;

import Class.DB.Factory.ArrayDBFactory;
import Class.DB.IDatabase;

public class ArrayOption extends DBOption {
    @Override
    public boolean isUser(String username, String password) {
        return true;
    }

    @Override
    public IDatabase createDB() {
        System.out.println("Base de datos ARRAY seleccionada correctamente!!");
        return new ArrayDBFactory().createDatabase();
    }
}
