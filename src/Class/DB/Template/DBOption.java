package Class.DB.Template;


import Class.DB.IDatabase;

public abstract class DBOption {
    public IDatabase selectDB() {
        String username = setName();
        String password = setPassword();
        return isUser(username, password) ? createDB() : null;
    }

    public abstract boolean isUser(String username, String password);

    public abstract IDatabase createDB();

    public String setName() {
        return "admin";
    }

    public String setPassword() {
        return "admin";
    }
}
