package Class.DB.Template;

import Class.DB.Factory.TextDBFactory;
import Class.DB.IDatabase;

public class TextOption extends DBOption{
    @Override
    public boolean isUser(String username, String password) {
        return true;
    }

    @Override
    public IDatabase createDB() {
        System.out.println("Base de datos TEXT seleccionada correctamente!!");
        return new TextDBFactory().createDatabase();
    }
}
