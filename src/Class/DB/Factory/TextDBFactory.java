package Class.DB.Factory;

import Class.DB.IDatabase;
import Class.DB.TextDB;

public class TextDBFactory extends DBFactory{
    @Override
    public IDatabase createDatabase() {
        return new TextDB();
    }
}
