package Class.DB.Factory;

import Class.DB.IDatabase;

public abstract class DBFactory {
    public abstract IDatabase createDatabase();
}
