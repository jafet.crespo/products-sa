package Class.DB.Factory;

import Class.DB.ArrayDB;
import Class.DB.IDatabase;

public class ArrayDBFactory extends DBFactory{
    @Override
    public IDatabase createDatabase() {
        return new ArrayDB();
    }
}
