package Class.DB;

import Class.Product.Composite.ProductDTO;

public interface IDatabase {
    void connect();
    void insert(ProductDTO product);
}
