package Class.Delivery.Strategy;

import Class.Delivery.IDelivery;

public class WorldwideDelivery implements IDelivery {
    @Override
    public double calculatePrice() {
        System.out.println("------- ENTREGA A NIVEL MUNDIAL --------");
        return 1000;
    }
}
