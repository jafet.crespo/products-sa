package Class.Delivery.Strategy;

import Class.Delivery.IDelivery;

public class NationwideDelivery implements IDelivery {
    @Override
    public double calculatePrice() {
        System.out.println("------- ENTREGA A NIVEL NACIONAL --------");
        return 100;
    }
}
