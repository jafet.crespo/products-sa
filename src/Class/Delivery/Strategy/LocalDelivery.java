package Class.Delivery.Strategy;

import Class.Delivery.IDelivery;

public class LocalDelivery implements IDelivery {
    @Override
    public double calculatePrice() {
        System.out.println("------- ENTREGA A NIVEL LOCAL --------");
        return 10;
    }
}
