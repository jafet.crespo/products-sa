package Class.Delivery;

public class DeliveryContext {
    private IDelivery delivery;
    public void setDelivery(IDelivery delivery) {
        this.delivery = delivery;
    }

    public void deliveryPrice(){
        double price = this.delivery.calculatePrice();
        System.out.println("El precio del envio es: " + price);
    }
}
