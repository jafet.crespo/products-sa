package Class.ProductNotify;

import Class.Product.Composite.IProduct;

public interface IProductObserver {
    void update(IProduct product, double discount);
}
