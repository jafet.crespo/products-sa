package Class.ProductNotify.Observer;

import Class.Product.Composite.IProduct;
import Class.ProductNotify.IProductObserver;

import java.util.ArrayList;

public class PublisherProducts {
    private final ArrayList<IProductObserver> observers;
    IProduct product;

    public PublisherProducts() {
        this.observers = new ArrayList<>();
    }

    public void setProduct(IProduct product) {
        this.product = product;
    }

    public void subscribe(IProductObserver observer) {
        observers.add(observer);
    }


    public void notifyObservers(double discount) {
        observers.forEach(observer -> observer.update(this.product, discount));
    }
}
