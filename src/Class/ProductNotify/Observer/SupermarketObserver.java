package Class.ProductNotify.Observer;

import Class.Product.Composite.IProduct;
import Class.ProductNotify.IProductObserver;

public class SupermarketObserver implements IProductObserver {

    @Override
    public void update(IProduct product, double discount) {
        System.out.println(product + " se encuentra disponible con un descuento del " + (discount - 0.5) + "%");
    }
}
