package Class.MessagesProduct.Decorator;

import Class.MessagesProduct.IProductMessage;
import Class.Product.Composite.IProduct;

public class SimpleProductMessage implements IProductMessage {
    @Override
    public void print(IProduct product) {
        product.getProduct();
    }
}
