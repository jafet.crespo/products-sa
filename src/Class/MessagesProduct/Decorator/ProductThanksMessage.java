package Class.MessagesProduct.Decorator;

import Class.MessagesProduct.IProductMessage;
import Class.Product.Composite.IProduct;

public class ProductThanksMessage implements IProductMessage {

    IProductMessage message;
    public ProductThanksMessage(IProductMessage message) {
        this.message = message;

    }

    @Override
    public void print(IProduct product) {
        System.out.println("Gracias por su compra!!!");
        this.message.print(product);
    }

}
