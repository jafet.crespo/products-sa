package Class.MessagesProduct;

import Class.Product.Composite.IProduct;

public interface IProductMessage {
    void print(IProduct product);
}
