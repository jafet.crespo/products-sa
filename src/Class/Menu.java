package Class;

import Class.Product.Composite.IProduct;
import Class.Product.Composite.ProductDTO;
import Class.Product.Composite.ProductDTOComposite;
import Class.Product.Composite.ProductGift;
import Class.Product.PBuilder.DirectorProduct;
import Class.Product.PBuilder.ProductDTOBuilder;
import Class.Services.*;
import Interface.IProductService;

import java.util.ArrayList;


public class Menu {

    IProductService productServiceProxy;
    PromotionService promotionService;
    ComboService comboService;
    DeliveryService deliveryService;
    NotifyService notifyService;
    DirectorProduct directorProduct;
    ProductDTOBuilder productDTOBuilder;
    PurchaseService purchaseService;

    public Menu(IProductService productService) {
        this.directorProduct = new DirectorProduct();
        this.productDTOBuilder = new ProductDTOBuilder();
        this.productServiceProxy = productService;
        this.promotionService = new PromotionService();
        this.comboService = new ComboService(createCombos());
        this.deliveryService = new DeliveryService();
        this.notifyService = new NotifyService();
        this.purchaseService = new PurchaseService();
    }

    public void menuScreen() {
        System.out.println("----------OPCIONES---------");
        System.out.println("1.- Agregar productos");
        System.out.println("2.- Mostrar descuento de promoción");
        System.out.println("3.- Ver combos");
        System.out.println("4.- Notificar a tiendas");
        System.out.println("5.- Mostrar precio de envio");
        System.out.println("6.- Mostrar métodos de compra");
        System.out.println("0 para Salir del sistema");
        System.out.println("Ingrese una opción: ");
    }

    public void menuOptions(int option) {
        switch (option) {
            case 1 -> createProduct();
            case 2 -> this.promotionService.selectPromotion();
            case 3 -> this.comboService.showAllCombos();
            case 4 -> notifyProduct();
            case 5 -> this.deliveryService.showDeliveryPrice();
            case 6 -> this.purchaseService.showPurchaseMethods();
            case 0 -> System.out.println("hasta pronto");
        }
    }

    public void notifyProduct() {
        this.directorProduct.defaultProduct(this.productDTOBuilder);
        ProductDTO newProduct = this.productDTOBuilder.getProduct();
        this.notifyService.notifyToStores(newProduct);
    }

    public void createProduct() {
        try {
            System.out.println("------------ CREANDO PRODUCTO -----------------");
            this.directorProduct.personalProduct(this.productDTOBuilder);
            ProductDTO newProduct = this.productDTOBuilder.getProduct();
            this.productServiceProxy.create(newProduct);
        } catch (Exception e) {
            System.out.println("Tipo de dato incorrecto!!");
        }
    }

    public ArrayList<IProduct> createCombos() {
        ProductDTOComposite combo1 = new ProductDTOComposite();
        this.directorProduct.defaultProduct(this.productDTOBuilder);
        ProductDTO newProduct = this.productDTOBuilder.getProduct();
        combo1.addProduct(newProduct);
        combo1.addProduct(newProduct);
        combo1.addProduct(new ProductGift("tarjeta", "este es la tarjeta del combo 1"));

        ProductDTOComposite combo2 = new ProductDTOComposite();
        combo2.addProduct(newProduct);
        combo2.addProduct(newProduct);
        combo2.addProduct(new ProductGift("tarjeta", "este es la tarjeta del combo 2"));

        ProductDTOComposite womboCombo = new ProductDTOComposite();
        womboCombo.addProduct(combo1);
        womboCombo.addProduct(combo2);

        ArrayList<IProduct> combos = new ArrayList<>();
        combos.add(combo1);
        combos.add(combo2);
        combos.add(womboCombo);
        return combos;
    }

}
