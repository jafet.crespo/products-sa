package Class.Services;

import Class.Delivery.DeliveryContext;
import Class.Delivery.Strategy.LocalDelivery;
import Class.Delivery.Strategy.NationwideDelivery;
import Class.Delivery.Strategy.WorldwideDelivery;

import java.util.Scanner;

public class DeliveryService {
    private final DeliveryContext deliveryContext;
    public DeliveryService() {
        this.deliveryContext = new DeliveryContext();
    }

    public void showDeliveryPrice(){
        System.out.println("1.- Entrega local");
        System.out.println("2.- Entrega a nivel nacional");
        System.out.println("3.- Entrega a nivel mundial");
        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        switch (option) {
            case 2 -> deliveryContext.setDelivery(new NationwideDelivery());
            case 3 -> deliveryContext.setDelivery(new WorldwideDelivery());
            default -> deliveryContext.setDelivery(new LocalDelivery());
        }
        deliveryContext.deliveryPrice();
    }
}
