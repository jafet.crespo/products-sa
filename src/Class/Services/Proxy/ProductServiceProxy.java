package Class.Services.Proxy;

import Class.DB.IDatabase;
import Class.Product.Composite.ProductDTO;
import Class.ProductValidators.AllProductValidators;
import Class.Services.ProductService;
import Interface.IProductService;

public class ProductServiceProxy implements IProductService {
    private final ProductService productService;
    private final String user;
    AllProductValidators validators;

    public ProductServiceProxy(String user, IDatabase database) {
        this.productService = new ProductService(database);
        this.user = user;
        this.validators = new AllProductValidators();
    }

    @Override
    public void create(ProductDTO product) {
        if (user.equals("admin") && validators.validateProduct(product)) {
            productService.create(product);
        } else {
            System.out.println("Acceso denegado!!");
        }
    }
}
