package Class.Services;

import Class.DB.IDatabase;
import Class.MessagesProduct.Decorator.ProductThanksMessage;
import Class.MessagesProduct.Decorator.SimpleProductMessage;
import Class.MessagesProduct.IProductMessage;
import Class.Product.Composite.ProductDTO;
import Interface.*;

public class ProductService implements IProductService {

    IDatabase products;
    IProductMessage message;


    public ProductService(IDatabase products) {
        this.products = products;
        this.message = new ProductThanksMessage(new SimpleProductMessage());
    }

    @Override
    public void create(ProductDTO product) {
        products.connect();
        products.insert(product);
        message.print(product);
    }

}
