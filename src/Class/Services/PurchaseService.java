package Class.Services;

import Class.Purchase.Adapter.FaceToFacePurchase;
import Class.Purchase.Adapter.OnlinePurchase;
import Class.Purchase.Adapter.ElectronicPurchaseAdapter;
import Class.Purchase.IPurchaseMethod;

import java.util.Scanner;

public class PurchaseService {
    IPurchaseMethod purchaseMethod;

    public void showPurchaseMethods() {
        System.out.println("------------METODOS DE COMPRA-------------");
        System.out.println("1.- Compra presencial");
        System.out.println("2.- Compra online");
        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        switch (option) {
            case 2 -> this.purchaseMethod = new ElectronicPurchaseAdapter(new OnlinePurchase());
            default -> this.purchaseMethod = new FaceToFacePurchase();
        }
        this.purchaseMethod.payTheSeller();
        this.purchaseMethod.receiveChangeBill();
    }
}
