package Class.Services;


import Class.Product.Composite.IProduct;
import Class.ProductNotify.Observer.GroceryStoreObserver;
import Class.ProductNotify.Observer.PublisherProducts;
import Class.ProductNotify.Observer.SupermarketObserver;

public class NotifyService {
    PublisherProducts publisherProducts;

    public NotifyService() {
        this.publisherProducts = new PublisherProducts();
        publisherProducts.subscribe(new GroceryStoreObserver());
        publisherProducts.subscribe(new SupermarketObserver());
    }

    public void notifyToStores(IProduct newProduct) {
        this.publisherProducts.setProduct(newProduct);
        this.publisherProducts.notifyObservers(12);
    }
}
