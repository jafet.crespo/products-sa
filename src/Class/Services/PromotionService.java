package Class.Services;

import Class.Promotion.Builder.DirectorPromotion;
import Class.Promotion.Builder.PromotionBuilder;
import Class.Promotion.Promotion;

import java.util.Scanner;

public class PromotionService {
    Promotion promotion;
    DirectorPromotion directorPromotion;
    PromotionBuilder promotionBuilder;

    public PromotionService() {
        this.directorPromotion = new DirectorPromotion();
        this.promotionBuilder = new PromotionBuilder();
    }

    public void selectPromotion() {
        System.out.println("1.- Promoción navideña");
        System.out.println("2.- Promoción halloween");
        System.out.println("0.- Sin ninguna promoción");
        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        switch (option) {
            case 1 -> this.directorPromotion.christmasPromotion(this.promotionBuilder);
            case 2 -> this.directorPromotion.halloweenPromotion(this.promotionBuilder);
            default -> this.directorPromotion.withoutPromotion(this.promotionBuilder);
        }
        this.promotion = promotionBuilder.getPromotion();
        this.promotion.showDiscount();
    }
}
