package Class.Services;


import Class.Product.Composite.IProduct;

import java.util.ArrayList;

public class ComboService {
    private final ArrayList<IProduct> combos;

    public ComboService(ArrayList<IProduct> combos) {
        this.combos = combos;
    }


    public void showAllCombos() {
        System.out.println("-----los combos de la tienda ---------");
        combos.forEach(combo -> {
            System.out.println("----combo----");
            combo.getProduct();
        });
    }
}
