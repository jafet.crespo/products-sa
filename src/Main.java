
import java.util.Scanner;

import Class.DB.Template.ArrayOption;
import Class.DB.Template.DBOption;
import Class.DB.Template.TextOption;
import Class.Menu;
import Class.Services.Proxy.ProductServiceProxy;
import Class.DB.IDatabase;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Seleccione una base de datos:");
        System.out.println("1.- Array");
        System.out.println("2.- text");
        int inputDB = scanner.nextInt(); // opción base de datos
        IDatabase dbOption = selectDBOption(inputDB).selectDB();
        int input; // opción del menu
        Menu menu = new Menu(new ProductServiceProxy("admin", dbOption));
        do {
            menu.menuScreen();
            input = scanner.nextInt();
            menu.menuOptions(input);
        } while (input != 0);
    }

    public static DBOption selectDBOption(int input){
        if (input == 2) {
            return new ArrayOption();
        } else {
            return new TextOption();
        }
    }
}