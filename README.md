# Products Sa

El proyecto tiene como objetivo cumplir con los siguientes aspectos:

- Agregar productos simples a una lista.
- Ver los descuentos de una promoción.
- Ver los combos disponibles en la tienda.
- Notificar a las tiendas sobre el descuento de un producto.
- Mostrar el precio standard de los envios.

Para cumplir con el proyecto se tiene el siguiente diagrama UML de clases:
![diagramaProductos](product-sa-diagram.png)